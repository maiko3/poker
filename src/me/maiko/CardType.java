package me.maiko;

public enum CardType {
    Spades, Hearts, Diamonds, Clubs
}