package me.maiko;

public class Main {

    public static void main(String[] args)  {
        Deck deck = new Deck();
        Table table = new Table(deck);
        System.out.println("Poker gestartet!");

        for (int x = 0 ; x < 5 ; x++) {
            Card revOne = table.revealCard();
            System.out.println("Eine Karte wurde aufgedeckt! " + revOne.value + " " + revOne.type.toString() );
        }
        System.out.println("Alle Hände werden jetzt aufgedeckt");
        Hand one = new Hand(deck.getRandomCard(), deck.getRandomCard());
        Hand two = new Hand(deck.getRandomCard(), deck.getRandomCard());
        System.out.println(one.card[0].value + " " + one.card[0].type + ", " + one.card[1].value + " " + one.card[1].type);
        System.out.println(two.card[0].value + " " + two.card[0].type + ", " + two.card[1].value + " " + two.card[1].type);
        System.out.println("Das Deck hat noch " + deck.current.size() + " Karten!");
    }
}
