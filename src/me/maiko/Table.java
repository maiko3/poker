package me.maiko;

public class Table {
    public Card[] cards = new Card[5];
    public int currentRevealed = -1;
    public Table(Deck deck) {
        for (int x = 0; x < 5; x++) {
            cards[x] = deck.getRandomCard();
        }
    }

    public Card revealCard() {
        if (currentRevealed == 4) {
            throw new ArrayIndexOutOfBoundsException("Der Tisch hat nur 5 Karten!");
        }
        currentRevealed++;
        return cards[currentRevealed];
    }
}
