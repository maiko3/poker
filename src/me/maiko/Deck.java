package me.maiko;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Deck {
    /**
     * Karten 2 bis 10 normal danach Bube: 11, Dame: 12, König: 13, Ass: 14
     */
    private Card[] fullAsArr = {
            new Card(2, CardType.Hearts),
            new Card(2, CardType.Spades),
            new Card(2, CardType.Diamonds),
            new Card(2, CardType.Clubs),
            new Card(3, CardType.Hearts),
            new Card(3, CardType.Spades),
            new Card(3, CardType.Diamonds),
            new Card(3, CardType.Clubs),
            new Card(4, CardType.Hearts),
            new Card(4, CardType.Spades),
            new Card(4, CardType.Diamonds),
            new Card(4, CardType.Clubs),
            new Card(5, CardType.Hearts),
            new Card(5, CardType.Spades),
            new Card(5, CardType.Diamonds),
            new Card(5, CardType.Clubs),
            new Card(6, CardType.Hearts),
            new Card(6, CardType.Spades),
            new Card(6, CardType.Diamonds),
            new Card(6, CardType.Clubs),
            new Card(7, CardType.Hearts),
            new Card(7, CardType.Spades),
            new Card(7, CardType.Diamonds),
            new Card(7, CardType.Clubs),
            new Card(8, CardType.Hearts),
            new Card(8, CardType.Spades),
            new Card(8, CardType.Diamonds),
            new Card(8, CardType.Clubs),
            new Card(9, CardType.Hearts),
            new Card(9, CardType.Spades),
            new Card(9, CardType.Diamonds),
            new Card(9, CardType.Clubs),
            new Card(10, CardType.Hearts),
            new Card(10, CardType.Spades),
            new Card(10, CardType.Diamonds),
            new Card(10, CardType.Clubs),
            new Card(11, CardType.Hearts),
            new Card(11, CardType.Spades),
            new Card(11, CardType.Diamonds),
            new Card(11, CardType.Clubs),
            new Card(12, CardType.Hearts),
            new Card(12, CardType.Spades),
            new Card(12, CardType.Diamonds),
            new Card(12, CardType.Clubs),
            new Card(13, CardType.Hearts),
            new Card(13, CardType.Spades),
            new Card(13, CardType.Diamonds),
            new Card(13, CardType.Clubs),
            new Card(14, CardType.Hearts),
            new Card(14, CardType.Spades),
            new Card(14, CardType.Diamonds),
            new Card(14, CardType.Clubs)
    };

    public ArrayList<Card> current = new ArrayList<>();
    public Deck() {
        current.addAll(Arrays.asList(fullAsArr));
    }

    public Card getRandomCard() {
        Random r = new Random();
        Card randomCard = current.get(r.nextInt(current.size()));
        current.remove(randomCard);
        return randomCard;
    }

}


