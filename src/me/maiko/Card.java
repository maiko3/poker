package me.maiko;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Random;

public class Card {
    public Integer value;
    public CardType type;
    public Deck curDeck;
    public Card(int value, CardType type) {
        this.value = value;
        this.type = type;
    }


    private Card(Deck deck) {
        curDeck = deck;
        CreateRandomCard();
    }
    private void CreateRandomCard() {
        Random r = new Random();
        int x = r.nextInt(1, 4);
        switch (x) {
            case 1:
                type = CardType.Spades;
                break;
            case 2:
                type = CardType.Hearts;
                break;
            case 3:
                type = CardType.Diamonds;
                break;
            case 4:
                type = CardType.Clubs;
                break;
        }
        value = r.nextInt(2, 14);
    }
}
